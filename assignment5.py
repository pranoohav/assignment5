#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

missing = ['4']

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return r

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        nam = request.form['added']
        iden = len(books) + 1
        if missing != []:
            iden = missing[0]
        tmp = {'title': nam, 'id': iden}
        done = 0
        for x in range(len(books)):
            if books[x]['id'] > str(iden):
                done = 1
                books.insert(x, tmp)
                break
        if done == 0:
            books.append(tmp)
    return render_template('newBook.html', books = books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    index = find(str(book_id))
    if request.method == 'POST':
        nam = request.form['name']                 
        books[index]["title"] = nam                    
    return render_template('editBook.html', id = index, books = books)    
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    index = find(str(book_id))   
    if request.method == 'POST':        
        for x in range(len(missing)):
            if missing[x] > str(book_id):
                missing.insert(x, str(book_id))
                break                            
        del books[index]
        return render_template('showBook.html', books = books)    
    return render_template('deleteBook.html', id = index, books = books)

def find(value):
    index = -1
    for x in books:
        index = index + 1        
        if x["id"] == value:
            return index
    return None  

if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 5000)
	

